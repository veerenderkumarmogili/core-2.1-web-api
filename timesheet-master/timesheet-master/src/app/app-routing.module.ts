import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Import the components so they can be referenced in routes
import { EmployeeListComponent } from './employee/employee.component';
import { CreateTimesheetComponent } from './timesheet/create-timesheet.component';
import { ListTimesheetComponent } from './timesheet/list-timesheet.component';

// The last route is the empty path route. This specifies
// the route to redirect to if the client side path is empty.
const appRoutes: Routes = [
  { path: 'list', component: EmployeeListComponent },
  { path: 'timesheet/:id', component: ListTimesheetComponent },
  { path: 'create/:id', component: CreateTimesheetComponent },
  { path: '', redirectTo: '/list', pathMatch: 'full' }
];

// Pass the configured routes to the forRoot() method
// to let the angular router know about our routes
// Export the imported RouterModule so router directives
// are available to the module that imports this AppRoutingModule
@NgModule({
  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
