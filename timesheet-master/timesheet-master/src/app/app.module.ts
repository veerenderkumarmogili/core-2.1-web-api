import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {RouterModule} from '@angular/router';
import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeService } from './services/employee.service';
import { TimesheetService } from './services/timesheet.service';
import { TaskService } from './services/tasks.service';
import { CalendarService } from './services/calendar.service';
import { CreateTimesheetComponent } from './timesheet/create-timesheet.component';
import { AppRoutingModule } from './/app-routing.module';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from "@angular/forms";
import { ListTimesheetComponent } from './timesheet/list-timesheet.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    CreateTimesheetComponent,
    ListTimesheetComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule,
    FormsModule,ReactiveFormsModule
  ],
  providers: [
    EmployeeService,TimesheetService,TaskService,CalendarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
