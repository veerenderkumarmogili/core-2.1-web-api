export class Timesheet{
    constructor(
                public Id:number,
                public TaskID:number,
                public EmployeeID:number,
                public WeekID:number,
                public Sunday:number,
                public Monday:number,
                public Tuesday:number,
                public Wednesday:number,
                public Thursday:number,
                public Friday:number,
                public Saturday:number,
                
                ){}
}