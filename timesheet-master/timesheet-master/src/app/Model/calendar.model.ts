export class Calendar{
    constructor(
                public Id:number,
                public WeekNumber:number,
                public Year:number,
                public Name:string,
                public Description:string 
                
                ){}
}