import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { TimesheetService } from '../services/timesheet.service';
import { TaskService } from '../services/tasks.service';
import { CalendarService } from '../services/calendar.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Timesheet } from './../Model/timesheet.model';
import { FormBuilder, FormGroup, FormControl, Validators, NgForm } from '@angular/forms';

@Component({
  selector: 'app-create-timesheet',
  templateUrl: './create-timesheet.component.html',
  styleUrls: ['./create-timesheet.component.scss']
})
export class CreateTimesheetComponent implements OnInit {

  weeks: any;
  tasks: any;
  timesheet: any;
  timesheetForm: FormGroup;
  id: number;
  constructor(private frmbuilder: FormBuilder, private router: Router, private timesheetService: TimesheetService, private taskService: TaskService, private employeeService: EmployeeService, private calendarService: CalendarService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.timesheetForm = new FormGroup({
      WeekID: new FormControl(Validators.required),
      TaskID: new FormControl(Validators.required),
      EmployeeID : new FormControl(Validators.required),
      Sunday: new FormControl(),
      Monday: new FormControl(),
      Tuesday: new FormControl(),
      Wednesday: new FormControl(),
      Thursday: new FormControl(),
      // Friday: new FormControl(),
      // Saturday: new FormControl(),
    });

    this.bindWeekList();
    this.bindTaskList();
    this.route.params.subscribe((params) => {
      this.id = params["id"];

      // this.bindemployeedata(id)
      //need to bind the current week by date
      this.timesheetForm.controls['WeekID'].setValue(8);
      this.timesheetForm.controls['EmployeeID'].setValue(this.id);
    });

  }

  public AddTimesheet() {
    console.log(this.timesheetForm);
    debugger;
   
    if (this.timesheetForm.value.Sunday > 0) {
      this.timesheetService.savetimesheet(this.timesheetForm.value).then(resp => {
        this.router.navigate(['/timesheet/' + this.id]);
      }).catch(exp => {
        console.log("Server Exception was raised");
      });
    }


  }





  bindWeekList() {
    this.calendarService.getallcalendars().subscribe(data => {
      this.weeks = data;
      console.log(this.weeks);
    });
  }

  bindTaskList() {
    this.taskService.getalltasks().subscribe(data => {
      this.tasks = data;
      console.log(this.weeks);
    });
  }


}
