import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { TimesheetService } from '../services/timesheet.service';
import { TaskService } from '../services/tasks.service';
import { CalendarService } from '../services/calendar.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { TimesheetRequest } from './../Model/timesheetRequest.model';
import { FormBuilder, FormGroup, FormControl, Validators, NgForm } from '@angular/forms';

@Component({
  selector: 'app-list-timesheet',
  templateUrl: './list-timesheet.component.html',
  styleUrls: ['./list-timesheet.component.scss']
})
export class ListTimesheetComponent implements OnInit {

  employee: any;
  employees:any;
  timesheets : any;
  TimesheetRequest: TimesheetRequest;
  weeks: any;
  id: number;
  EmployeeForm: FormGroup;
  constructor(private frmbuilder: FormBuilder, private timesheetService: TimesheetService,private employeeService: EmployeeService, private calendarService : CalendarService, private route: ActivatedRoute,private router : Router) { }

  ngOnInit() {
    this.EmployeeForm = new FormGroup({
      ddlEmployee: new FormControl() ,
      ddlWeek: new FormControl() 
    });
    this.bindEmployeeList();
    this.bindemployee();
    this.bindWeekList();
   
  }

  bindemployee() {

    // this.EmployeeForm = this.frmbuilder.group({
      
    //   ddlEmployee: [''], 
    // })
    this.route.params.subscribe((params) => {
      this.id = params["id"];
 
      this.EmployeeForm.controls['ddlEmployee'].setValue(this.id );
      //this.bindemployeedata(this.id)
     //need to bind the current week by date
     this.EmployeeForm.controls['ddlWeek'].setValue(8);
     this.bindtimesheetdata();
    });
    //this.fileInputDaily.nativeElement.attribute.disable = 'disabled';
  }

  bindemployeedata(id) {
    this.employeeService.getemployee(id).subscribe(data => {
      this.employee = data;
      console.log(this.employee);
    });
  }

  bindtimesheetdata(){
    
    this.TimesheetRequest = new TimesheetRequest(this.id,this.EmployeeForm.value.ddlWeek);
    console.log(this.TimesheetRequest );
    this.timesheetService.getalltimesheetsbyEmpandWeek(this.TimesheetRequest).subscribe(data => {
      this.timesheets = data;
      console.log(this.timesheets);
    });
  }

  bindEmployeeList(){
    this.employeeService.getallemployees().subscribe(data => {
      this.employees = data;
      console.log(this.employees);
  });
  }

  bindWeekList(){
    this.calendarService.getallcalendars().subscribe(data => {
      this.weeks = data;
      console.log(this.weeks);
  });
  }

  changeEmployee(filterVal: any) { 
    this.router.navigate(['/timesheet/'+ filterVal ]);
  }
  changeWeek(filterVal: any) { 
      console.log(filterVal);
      this.bindtimesheetdata();
  }
  CreateTimesheet(){
    this.router.navigate(['/create/'+ this.id ]);
  }
}
