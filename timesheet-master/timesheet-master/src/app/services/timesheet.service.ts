import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Timesheet } from './../Model/timesheet.model';
import { TimesheetRequest } from './../Model/timesheetRequest.model';


@Injectable()
export class TimesheetService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getalltimesheets() {
        return this.http.get(this.baseapi + "/timesheet/getall");
    }
    savetimesheet(Timesheet : Timesheet){
        return this.http.post(this.baseapi + "/timesheet/", Timesheet)
        .toPromise();
    }

    getalltimesheetsbyEmpandWeek(TimesheetRequest) {
        return this.http.post(this.baseapi + "/timesheet/GetWeekly", TimesheetRequest);
    }
}