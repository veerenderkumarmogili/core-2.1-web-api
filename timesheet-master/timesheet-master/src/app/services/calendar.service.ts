import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CalendarService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getallcalendars() {
        return this.http.get(this.baseapi + "/calendar/getall");
    }
}