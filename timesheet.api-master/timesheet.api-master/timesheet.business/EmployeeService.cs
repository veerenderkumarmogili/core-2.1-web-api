﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public  IList<EmployeeViewModel> GetEmployees()
        {
           // return this.db.Employees;
            
 

            var employees = this.db.Employees;
            var timesheets = this.db.Timesheets;
            IList<EmployeeViewModel> vm = new List<EmployeeViewModel>();
            foreach (var e in employees)
            {
                EmployeeViewModel vmm = new EmployeeViewModel();
                var sheets = timesheets.Where(i => i.EmployeeID == e.Id);
                int totalcost = 0;
                int count = 0;
                foreach (var item in sheets)
                {
                    totalcost = item.Monday + item.Saturday + item.Sunday + item.Thursday + item.Tuesday + item.Wednesday + item.Friday;
                    count = count + 1;
                }

                vmm.Id = e.Id;
                vmm.Name = e.Name;
                if(totalcost > 0 && count > 0)
                {
                    vmm.WeeklyAvg = totalcost / count;
                }
               
                vmm.WeeklyTotal = totalcost;

                vm.Add(vmm);

            }

            return vm;
        }

    

        public IQueryable<Employee> GetEmployee(int id)
        {
            return this.db.Employees.Where(i => i.Id == id);
        }
    }
}
