﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
     
    public class CalendarService
    {
        public TimesheetDb db { get; }
        public CalendarService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<Calendar> GetCalendars()
        {
            return this.db.Calendars;
        }

    }
}
