﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using timesheet.data;
using timesheet.model;
using Task = timesheet.model.Task;

namespace timesheet.business
{
   
    public class TaskService
    {
        public TimesheetDb db { get; }
        public TaskService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public  IQueryable<Task> GetTasks()
        {
            return  this.db.Tasks;
        }
    }
}
