﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{

    public class TimesheetService
    {
        public TimesheetDb db { get; }
        public TimesheetService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }



        public async Task<IList<Timesheet>> GetTimesheets()
        {
            return await this.db.Timesheets.ToListAsync();
        }

        public async Task<IList<TimesheetViewModel>> GetTimesheetByEmp(int empID)
        {

            var query = from ts in this.db.Timesheets
                        join tk in this.db.Tasks on ts.TaskID equals tk.Id
                        join wk in this.db.Calendars on ts.WeekID equals wk.Id
                        where ts.EmployeeID == empID
                        select new TimesheetViewModel
                        {
                            TaskID = ts.TaskID,
                            TaskName = tk.Name,
                            WeekName = wk.Name,
                            EmployeeID = ts.EmployeeID,
                            WeekID = ts.WeekID,
                            Sunday = ts.Sunday,
                            Monday = ts.Monday,
                            Tuesday = ts.Tuesday,
                            Wednesday = ts.Wednesday,
                            Thursday = ts.Thursday,
                            Friday = ts.Friday,
                            Saturday = ts.Saturday
                        }; 

            return await query.ToListAsync();


        }

        public async Task<IList<TimesheetViewModel>> GetTimesheetByEmp(int empID,int weekID)
        {

            var query = from ts in this.db.Timesheets
                        join tk in this.db.Tasks on ts.TaskID equals tk.Id
                        join wk in this.db.Calendars on ts.WeekID equals wk.Id
                        where ts.EmployeeID == empID && ts.WeekID == weekID
                        select new TimesheetViewModel
                        {
                            TaskID = ts.TaskID,
                            TaskName = tk.Name,
                            WeekName = wk.Name,
                            EmployeeID = ts.EmployeeID,
                            WeekID = ts.WeekID,
                            Sunday = ts.Sunday,
                            Monday = ts.Monday,
                            Tuesday = ts.Tuesday,
                            Wednesday = ts.Wednesday,
                            Thursday = ts.Thursday,
                            Friday = ts.Friday,
                            Saturday = ts.Saturday
                        };

            return await query.ToListAsync();


        }

        public async Task<int> AddTimesheet(Timesheet timesheet)
        {

            this.db.Timesheets.Add(timesheet);
            await this.db.SaveChangesAsync();

            //this.Commit();
            return timesheet.Id;
        }


        public async Task<Timesheet> UpdateTimesheet(int id, Timesheet timesheet)
        {

            this.db.Entry(timesheet).State = EntityState.Modified;

            try
            {
                await this.db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return timesheet;
        }

        private bool TimesheetExists(int id)
        {
            return this.db.Timesheets.Any(e => e.Id == id);
        }
    }
}
