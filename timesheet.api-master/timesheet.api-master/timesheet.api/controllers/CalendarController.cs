﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;

namespace timesheet.api.controllers
{
    [Route("api/v1/calendar")]
    [ApiController]
    public class CalendarController : ControllerBase
    {
        private readonly CalendarService calendarService;
        public CalendarController(CalendarService calendarService)
        {
            this.calendarService = calendarService;
        }


        [HttpGet("getall")]
        public IActionResult GetAll()
        {
            var items = this.calendarService.GetCalendars();
            return new ObjectResult(items);
        }

    }
}