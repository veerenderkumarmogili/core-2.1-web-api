﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/timesheet")]
    [ApiController]
    public class TimesheetController : ControllerBase
    {
        private readonly TimesheetService timesheetService;
        public TimesheetController(TimesheetService timesheetService)
        {
            this.timesheetService = timesheetService;
        }

        //[HttpGet("{id}")]
        //public async Task<ActionResult<IEnumerable<TimesheetViewModel>>>  GetTimesheet(int id)
        //{
        //    var timesheetDetail = await this.timesheetService.GetTimesheetByEmp(id);

        //    if (timesheetDetail == null)
        //    {
        //        return NotFound();
        //    }

        //    return timesheetDetail.ToList();
        //}

        [HttpPost("GetWeekly")]
        public async Task<ActionResult<IEnumerable<TimesheetViewModel>>> GetTimesheet(TimesheetRequest TimesheetRequest)
        {
            var timesheetDetail = await this.timesheetService.GetTimesheetByEmp(TimesheetRequest.EmployeeID, TimesheetRequest.WeekID);

            if (timesheetDetail == null)
            {
                return NotFound();
            }

            return timesheetDetail.ToList();
        }

        [HttpPost]
        public async Task<ActionResult<Timesheet>> AddTimesheet(Timesheet timesheet)
        {

            await this.timesheetService.AddTimesheet(timesheet);           

            return CreatedAtAction("AddTimesheet", new { id = timesheet.Id }, timesheet);
        }
    }
}