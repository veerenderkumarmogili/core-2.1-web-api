﻿using System;
using System.Collections.Generic;
using System.Text;

namespace timesheet.model
{
    public class TimesheetViewModel
    {
        public int Id { get; set; }

        public int TaskID { get; set; }
        public string TaskName { get; set; }
        public string WeekName { get; set; }
        public int EmployeeID { get; set; }

        public int WeekID { get; set; }

        public int Sunday { get; set; }
        public int Monday { get; set; }
        public int Tuesday { get; set; }
        public int Wednesday { get; set; }
        public int Thursday { get; set; }
        public int Friday { get; set; }
        public int Saturday { get; set; }

    }
}
