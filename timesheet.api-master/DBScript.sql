USE [master]
GO
/****** Object:  Database [TimeSheet]    Script Date: 2/28/2019 8:23:59 PM ******/
CREATE DATABASE [TimeSheet]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TimeSheet', FILENAME = N'C:\Users\Veer\TimeSheet.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'TimeSheet_log', FILENAME = N'C:\Users\Veer\TimeSheet_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [TimeSheet] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TimeSheet].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TimeSheet] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TimeSheet] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TimeSheet] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TimeSheet] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TimeSheet] SET ARITHABORT OFF 
GO
ALTER DATABASE [TimeSheet] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [TimeSheet] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TimeSheet] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TimeSheet] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TimeSheet] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TimeSheet] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TimeSheet] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TimeSheet] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TimeSheet] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TimeSheet] SET  ENABLE_BROKER 
GO
ALTER DATABASE [TimeSheet] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TimeSheet] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TimeSheet] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TimeSheet] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TimeSheet] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TimeSheet] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [TimeSheet] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TimeSheet] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TimeSheet] SET  MULTI_USER 
GO
ALTER DATABASE [TimeSheet] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TimeSheet] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TimeSheet] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TimeSheet] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [TimeSheet] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [TimeSheet] SET QUERY_STORE = OFF
GO
USE [TimeSheet]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [TimeSheet]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 2/28/2019 8:24:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Calendars]    Script Date: 2/28/2019 8:24:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calendars](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WeekNumber] [int] NOT NULL,
	[Year] [int] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_Calendars] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employees]    Script Date: 2/28/2019 8:24:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tasks]    Script Date: 2/28/2019 8:24:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tasks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_Tasks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Timesheets]    Script Date: 2/28/2019 8:24:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Timesheets](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TaskID] [int] NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[WeekID] [int] NOT NULL,
	[Sunday] [int] NOT NULL,
	[Monday] [int] NOT NULL,
	[Tuesday] [int] NOT NULL,
	[Wednesday] [int] NOT NULL,
	[Thursday] [int] NOT NULL,
	[Friday] [int] NOT NULL,
	[Saturday] [int] NOT NULL,
 CONSTRAINT [PK_Timesheets] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190109115334_initial', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190109181036_employee_seed', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190110054637_task_table', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190110054720_task_seed', N'2.1.8-servicing-32085')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190228110056_added_Timesheet_Calender', N'2.1.8-servicing-32085')
SET IDENTITY_INSERT [dbo].[Calendars] ON 

INSERT [dbo].[Calendars] ([Id], [WeekNumber], [Year], [Name], [Description]) VALUES (1, 1, 2019, N'Jan 1st Week', NULL)
INSERT [dbo].[Calendars] ([Id], [WeekNumber], [Year], [Name], [Description]) VALUES (2, 2, 2019, N'Jan 2nd Week', NULL)
INSERT [dbo].[Calendars] ([Id], [WeekNumber], [Year], [Name], [Description]) VALUES (3, 3, 2019, N'Jan 3rd Week', NULL)
INSERT [dbo].[Calendars] ([Id], [WeekNumber], [Year], [Name], [Description]) VALUES (4, 4, 2019, N'Jan 4th Week', NULL)
INSERT [dbo].[Calendars] ([Id], [WeekNumber], [Year], [Name], [Description]) VALUES (5, 6, 2019, N'Feb 1st Week', NULL)
INSERT [dbo].[Calendars] ([Id], [WeekNumber], [Year], [Name], [Description]) VALUES (6, 7, 2019, N'Feb 2nd Week', NULL)
INSERT [dbo].[Calendars] ([Id], [WeekNumber], [Year], [Name], [Description]) VALUES (7, 8, 2019, N'Feb 3rd Week', NULL)
INSERT [dbo].[Calendars] ([Id], [WeekNumber], [Year], [Name], [Description]) VALUES (8, 9, 2019, N'Feb 4th Week', NULL)
SET IDENTITY_INSERT [dbo].[Calendars] OFF
SET IDENTITY_INSERT [dbo].[Employees] ON 

INSERT [dbo].[Employees] ([Id], [Code], [Name]) VALUES (1, N'68319', N'KAYLING')
INSERT [dbo].[Employees] ([Id], [Code], [Name]) VALUES (2, N'66928', N'BLAZE')
INSERT [dbo].[Employees] ([Id], [Code], [Name]) VALUES (3, N'67832', N'CLARE')
INSERT [dbo].[Employees] ([Id], [Code], [Name]) VALUES (4, N'69062', N'JONAS')
INSERT [dbo].[Employees] ([Id], [Code], [Name]) VALUES (5, N'63679', N'SCARLET')
INSERT [dbo].[Employees] ([Id], [Code], [Name]) VALUES (6, N'64989', N'FRANK')
INSERT [dbo].[Employees] ([Id], [Code], [Name]) VALUES (7, N'65271', N'SANDRINE')
INSERT [dbo].[Employees] ([Id], [Code], [Name]) VALUES (8, N'66564', N'ADELYN')
INSERT [dbo].[Employees] ([Id], [Code], [Name]) VALUES (9, N'68454', N'WADE')
INSERT [dbo].[Employees] ([Id], [Code], [Name]) VALUES (10, N'69000', N'MADDEN')
SET IDENTITY_INSERT [dbo].[Employees] OFF
SET IDENTITY_INSERT [dbo].[Tasks] ON 

INSERT [dbo].[Tasks] ([Id], [Name], [Description]) VALUES (1, N'Sick Leave', N'Apply this task on sick leave.')
INSERT [dbo].[Tasks] ([Id], [Name], [Description]) VALUES (2, N'Scrum Ceremonies', N'Scrum meetings, standup, sprint plannig, grooming etc.')
INSERT [dbo].[Tasks] ([Id], [Name], [Description]) VALUES (3, N'Internal Meeting', N'Meetings Meetings.')
INSERT [dbo].[Tasks] ([Id], [Name], [Description]) VALUES (4, N'Development', N'Development tasks, features, change requets.')
INSERT [dbo].[Tasks] ([Id], [Name], [Description]) VALUES (5, N'Bug Fixes', N'You know what it means.')
SET IDENTITY_INSERT [dbo].[Tasks] OFF
SET IDENTITY_INSERT [dbo].[Timesheets] ON 

INSERT [dbo].[Timesheets] ([Id], [TaskID], [EmployeeID], [WeekID], [Sunday], [Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [Saturday]) VALUES (1, 2, 1, 8, 1, 2, 3, 3, 5, 0, 0)
INSERT [dbo].[Timesheets] ([Id], [TaskID], [EmployeeID], [WeekID], [Sunday], [Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [Saturday]) VALUES (2, 3, 1, 7, 4, 5, 6, 7, 8, 0, 0)
SET IDENTITY_INSERT [dbo].[Timesheets] OFF
USE [master]
GO
ALTER DATABASE [TimeSheet] SET  READ_WRITE 
GO
